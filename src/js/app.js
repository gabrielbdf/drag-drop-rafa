$(document).ready(function(){

    $('html').click(function() {
        $('#menucontainer').removeClass('show'); 
    });

    $('#menuwrap').click(function(event){
        event.stopPropagation();
    });

    $('.nav-bt').click(function() {
        $('.menu').toggleClass('show'); 
    });

    // $('.menu-bt').click(function() {
    //     $(this).next().toggleClass('hide');
    // });

    $('#uI').click(function() {
        $('#uIsub').toggleClass('show');
        $('#uIIsub').removeClass('show');
        $('#uIIIsub').removeClass('show');
    });
    $('#uII').click(function() {
        $('#uIIsub').toggleClass('show');
        $('#uIsub').removeClass('show');
        $('#uIIIsub').removeClass('show');
    });
    $('#uIII').click(function() {
        $('#uIIIsub').toggleClass('show');
        $('#uIsub').removeClass('show');
        $('#uIIsub').removeClass('show');
    });

    $('div.botao a').click(function() {
        $(this).parent().toggleClass('acordeao');
    });

    $('#info-go-trigger1').click(function() {
        $('#info-go1').toggleClass('show');
    });

    $('#info-go-trigger2').click(function() {
        $('#info-go2').toggleClass('show');
    });

    $('#info-go-trigger3').click(function() {
        $('#info-go3').toggleClass('show');
    });

    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top+10)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    
    var wheight = $(window).height(); //get the height of the window

    if ($(window).width() > 800) {
        $('.fullheight').css('height', wheight);
    }

    $(window).resize(function() {
        if ($(window).width() > 800) {
            $('.fullheight').css('height', wheight);
        } else {
            $('.fullheight').removeAttr('style');
        }
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({ target: '.navbar', offset: 0 });

    // Offset for Main Navigation
    $('.navbar').affix({ offset: { top: wheight} });

    $(".pop-top").popover({
        placement : 'top'
    });
    $(".pop-right").popover({
        placement : 'right'
    });
    $(".pop-bottom").popover({
        placement : 'bottom'
    });
    $(".pop-left").popover({
        placement : 'left'
    });

    $('[data-toggle="popover"],[data-original-title]').popover({html:true});

    $(document).on('click', function(e) {
        $('[data-toggle="popover"],[data-original-title]').each(function() {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
            }

        });
    });

    $('.hemocomponentes').onScreen({
        visible: function() {
            $('.hemocomponentes figure').addClass("animacao");
        },
        hidden: function() {
            $('.hemocomponentes figure').removeClass("animacao");
        }
    });

    $('.change-menu-atividades').onScreen({
        visible: function() {
            $('.navbar').addClass("affix-atividades");
        }
    });

    $('.unchange-menu-atividades').onScreen({
        visible: function() {
            $('.navbar').removeClass("affix-atividades");
        }
    });


    $('h1').onScreen({
        visible: function() {
            $(this).addClass("pulse");            
        },
        hidden: function() {
            $(this).removeClass("pulse");
        }
    });

    $('.smartphone-conteiner').onScreen({
        visible: function() {
            $(this).addClass("shake");            
        },
        hidden: function() {
            $(this).removeClass("shake");
        }
    });

    $('#fornecimento_hemocomponentes a').click(function(){
        $(this).toggleClass('checked');
        
        if(!$('#uti').hasClass('checked') && !$('#urgencia').hasClass('checked') && !$('#altaComplexidade').hasClass('checked') && !$('#referencia').hasClass('checked')) {
            console.log('nenhum tem')
            $('#campo-total').text('3 a 5'),
            $('#campo-media').text('4');
        } else if ($('#uti').hasClass('checked') && !$('#urgencia').hasClass('checked') && !$('#altaComplexidade').hasClass('checked') && !$('#referencia').hasClass('checked')){
            console.log('uti tem')
            $('#campo-total').text('6 a 9'),
            $('#campo-media').text('8');
        } else if (!$('#uti').hasClass('checked') && $('#urgencia').hasClass('checked') && !$('#altaComplexidade').hasClass('checked') && !$('#referencia').hasClass('checked')){
            console.log('uti e tem')
            $('#campo-total').text('6 a 9'),
            $('#campo-media').text('8');
        } else if ($('#uti').hasClass('checked') && $('#urgencia').hasClass('checked') && !$('#altaComplexidade').hasClass('checked') && !$('#referencia').hasClass('checked')){
            console.log('uti tem')
            $('#campo-total').text('10 a 15'),
            $('#campo-media').text('13');
        } else if ($('#altaComplexidade').hasClass('checked') && !$('#referencia').hasClass('checked')){
            console.log('altaComplexidade tem')
            $('#uti').addClass('checked'),
            $('#urgencia').addClass('checked'),
            $('#campo-total').text('16 a 20'),
            $('#campo-media').text('17');
        } else if ($('#referencia').hasClass('checked')){
            console.log('ssdasa')
            $('#uti').addClass('checked'),
            $('#urgencia').addClass('checked'),
            $('#altaComplexidade').addClass('checked'),
            $('#campo-total').text('21 a 50'),
            $('#campo-media').text('30');
        } else {
            console.log('tem')
        }
    });
});

window.sr = ScrollReveal({ reset: true, mobile: false});
sr.reveal('.unidade');
sr.reveal('.modulo', 400);
sr.reveal('.silueta', { duration: 800, distance: '2000px', reset: false});
sr.reveal('.talk-bubble', { delay: 400, origin: 'top' });
sr.reveal('.conteudo>blockquote', { scale: 2, origin: 'left', distance: '2000px'});
sr.reveal('.linha-do-tempo a');
sr.reveal('.figura');

// sr.reveal('.conteudo>blockquote>.icone', { scale: 2, origin: 'left', distance: '2000px'}, 10);
// sr.reveal('.smartphone-conteiner', {duration: 800, rotate: { x: 30, y: 0, z: 30 }, distance: '100px'});
// sr.reveal('.infografico-campo', { origin: 'top' }, 20);
// sr.reveal('.conteudo h1', { duration: 800, scale: 2, origin: 'left', distance: '2000px'});




