$(function () {
    window.opccoesData = [];
    $.getJSON("json/opcoes.json", function (data) {
        opccoesData = data;
        var drag_drop_opcoes = document.createElement("div");
        drag_drop_opcoes.classList.add("drag-drop-opcoes");
        data2 = data.slice();
        shuffle(data2);
        data2.forEach(function (element) {
            var drag_drop_opcao = document.createElement("div");
            drag_drop_opcao.classList.add("drag-drop-opcao");
            var drag_drop_content = document.createElement("div");
            drag_drop_content.classList.add("drag-drop-content");
            drag_drop_content.setAttribute("opcao", element.opcao);
            drag_drop_content.innerHTML = element.texto;
            drag_drop_opcao.appendChild(drag_drop_content);
            drag_drop_opcoes.appendChild(drag_drop_opcao);
        });
        document.getElementById("opcoes").appendChild(drag_drop_opcoes);
        listenerMethods();
    });
    $('#refresh').click(function () {
        location.reload();
    });
});


function listenerMethods() {
    $(".drag-drop-opcao").draggable({
        helper: "clone",
        containment: "document",
        cursorAt: {
            top: 50,
            left: 50
        },
        cursor: 'move',
        zIndex: 99999,
        appendTo: "body",
    });

    $(".drag-drop-diagrama-campo").droppable({
        accept: '.drag-drop-opcao',
        drop: function (event, ui) {
            var elemento = ui.draggable.children(".drag-drop-content");
            var optDrop = elemento.attr("opcao");
            var optAceita = $(this).attr("aceita");
            if (optDrop == optAceita) {
                opccoesData.forEach(function (element) {
                    if (element.opcao == optDrop) {
                        element.correto = true;
                    }
                });
                $(this).append('<i class="glyphicon glyphicon-ok"></i>');
            } else {
                opccoesData.forEach(function (element) {
                    if (element.opcao == optDrop) {
                        element.correto = false;
                    }
                });
                $(this).append('<i class="glyphicon glyphicon-remove"></i>');
            }
            ui.draggable.children(".drag-drop-content").appendTo($(this));
            ui.draggable.detach();
            $(this).droppable("disable");
        },
    });
}


function checarRespostas() {
    var falta = false;
    opccoesData.forEach(function (element) {
        if (!element.hasOwnProperty("correto")) {
            falta = true;
        }
    });
    if (falta) {
        $('#preencher').modal('show');
    } else {
        var erros = 0;
        var acertos = 0;

        document.getElementById("respostas").innerHTML = "";
        var body = document.createElement("tbody");
        opccoesData.forEach(function (element) {
            var tr = document.createElement("tr");
            var td1 = document.createElement("td");
            td1.innerHTML = "#" + element.opcao;
            var td2 = document.createElement("td");
            td2.classList.add("questao");
            td2.innerHTML = element.texto;
            var td3 = document.createElement("td");
            td3.classList.add("feedback-icon");
            var i = document.createElement("i");
            i.classList.add("glyphicon");
            td3.appendChild(i);
            var td4 = document.createElement("td");
            if (element.correto) {
                tr.classList.add("correto");
                i.classList.add("glyphicon-ok");
                td4.innerHTML = element.acerto;
                acertos++;
            } else {
                tr.classList.add("incorreto");
                i.classList.add("glyphicon-remove");
                td4.innerHTML = element.erro;
                erros++
            }
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            body.appendChild(tr);
        });
        document.getElementById("respostas").appendChild(body);
        $('.drag-drop-diagrama-campo').addClass('verificar');
        if (erros == opccoesData.length) {
            $('#errou-tudo').modal('show');
        } else {
            $('#feedback1').modal('show');
        }

    }

}


function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}